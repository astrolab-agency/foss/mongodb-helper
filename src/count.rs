use bson::Document;
use mongodb::options::CountOptions;

impl super::Database {
    pub async fn count(
        &self,
        collection: &str,
        filter: Option<Document>,
        count_options: Option<CountOptions>,
    ) -> Result<i64, String> {
        let collection = self.db.collection(&String::from(collection));

        match collection.count_documents(filter, count_options).await {
            Ok(r) => return Ok(r),
            Err(e) => return Err(e.to_string()),
        }
    }
}
